function FindProxyForURL(url, host) {
    // Tor config
    var tor_proxy = '192.168.1.10:9050';
    // I2P config
    var i2p_proxy = '192.168.1.10:4444';


    // Check if host is in the local network
    if (
        isInNet(host, "10.0.0.0", "255.0.0.0") ||
        isInNet(host, "172.16.0.0", "255.240.0.0") ||
        isInNet(host, "192.168.0.0", "255.255.0.0")
    ) {
        return "DIRECT";
    }

    // Tor-only address list
    var tor_list = new Array(

    );

    // I2P-only address list
    var i2p_list = new Array(

    );

    // Checking if address is belonging to Tor-only list
    for(var i = 0; i < tor_list.length; i++) {
        var value = tor_list[i];
        if (localHostOrDomainIs(host, value)) {
            return 'SOCKS5 ' + tor_proxy;
        }
    }

    // Checking if address is belonging to I2P-only list
    for(var i = 0; i < i2p_list.length; i++) {
        var value = i2p_list[i];
        if (localHostOrDomainIs(host, value)) {
            return 'PROXY ' + i2p_proxy;
        }
    }

    // If domain zone is '.onion' or address is not resolvable
    if (dnsDomainIs(host, '.onion')/* || !isResolvable(host)*/) {
        return 'SOCKS5 ' + tor_proxy;
    }

    // If domain zone is '.i2p'
    if (dnsDomainIs(host, '.i2p')) {
        return 'PROXY ' + i2p_proxy;
    }

    return "DIRECT";    
}